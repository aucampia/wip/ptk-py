# ...

```bash
poetry run aucampia.template.poetry
poetry run aucampia.template.poetry.click
```

```bash
poetry install
poetry run ia-tk-py

make help
make validate-fix validate
```


```
mkdir -vp /usr/share/fkeeper-v0/
git clone git@gitlab.com:aucampia/fkeeper.git /usr/share/fkeeper-v0/repo

mkdir -vp ~/.local/share/fkeeper-v0/
git clone git@gitlab.com:aucampia/fkeeper.git ~/.local/share/fkeeper-v0/repo

```

```
docker compose run --rm python-devtools make help
docker compose run --rm python-devtools make validate-fix validate
```

## Updating from template base

```bash
pipx run --spec=cruft cruft update
```

```bash
pipx install --force --editable ~/sw/d/gitlab.com/aucampia/wip/ptk-py
```
