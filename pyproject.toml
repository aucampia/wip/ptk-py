# https://python-poetry.org/docs/pyproject/
# https://www.python.org/dev/peps/pep-0621/
[tool.poetry]
name = "aucampia.toolkit"
version = "0.0.0"
description = ""
authors = ["Iwan Aucamp <aucampia@gmail.com>"]
license = "CC0-1.0"
readme = "README.md"
packages = [
    {include = "*", from = "src"},
]

[tool.poetry.scripts]
"ia-tk-py" = "aucampia.toolkit.cli:main"
"ftf" = "aucampia.toolkit.ftf:main"
"fkeeper"= "aucampia.toolkit.fkeeper:main"
"uritk" = "aucampia.toolkit.uritk:main"
"notes" = "aucampia.toolkit.notes.cli:main"
"fmeta"= "aucampia.toolkit.fmeta:main"
"m3utk" = "aucampia.toolkit.m3utk:main"

[tool.poetry.dependencies]
python = "^3.11"
BTrees = "^5.1"
click = "^8.1.7"
dacite = "^1.8.1"
PyYAML = "^6.0.1"
xdg = "^6.0.0"
pydantic = "^2.5.3"
typer = "^0.9.0"
GitPython = "^3.1.40"
pydantic-settings = "^2.1.0"
m3u-parser = "^0.4.1"


[tool.poetry.group.dev.dependencies]
black = "^23.12.1"
coverage = "^7.4.0"
mypy = "^1.8.0"
pre-commit = "^3.6.0"
pytest = "^7.4.4"
pytest-cov = "^4.1.0"
pytest-subtests = "^0.11.0"
typing-extensions = "^4.9.0"
types-dataclasses = "^0.6.6"
types-click = "^7.1.8"
types-PyYAML = "^6.0.12.12"
pip-audit = "^2.6.2"
ruff = "^0.1.9"


[tool.codespell]
ignore-words = ".codespellignore"

[tool.ruff]
# https://beta.ruff.rs/docs/configuration/
preview = true
target-version = "py311"
select = [
    "E", # pycodestyle errors
    "W", # pycodestyle warnings
    "F", # Pyflakes
    "I", # isort
    "N", # pep8-naming
    "RUF", # Ruff
    "UP", # pyupgrade
    "FA", # flake8-future-annotations
]

ignore = [
    "E501", # line too long:
    # Disabled based on black recommendations
    # https://black.readthedocs.io/en/stable/faq.html#why-are-flake8-s-e203-and-w503-violated
    "E203", # whitespace before ':'
    "E231", # missing whitespace after ','
    "E251", # Unexpected spaces around keyword / parameter equals
    # Disabled because this targets Python 3.9 or later
    "UP007", # Use `X | Y` for type annotations
]

extend-exclude = [
    "examples/**",
]

# Same as Black.
line-length = 88

[tool.coverage.report]
# https://coverage.readthedocs.io/en/coverage-5.0/config.html
show_missing = true

[tool.isort]
# https://pycqa.github.io/isort/docs/configuration/config_files.html
profile = "black"
src_paths = ["src", "tests"]

[tool.pytest.ini_options]
testpaths = ["tests"]
addopts = ["--cov-config=pyproject.toml", "--cov=src"]
# https://docs.pytest.org/en/stable/customize.html
# https://docs.pytest.org/en/stable/reference.html#configuration-options
log_format = "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
log_date_format = "%Y-%m-%dT%H:%M:%S"
log_cli_format = "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s %(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
log_cli_date_format = "%Y-%m-%dT%H:%M:%S"

[tool.mypy]
# https://mypy.readthedocs.io/en/stable/config_file.html
files = "src,tests"
mypy_path = "src"
python_version = "3.11"
strict = true
warn_unreachable = true
warn_unused_configs = true
explicit_package_bases = true
namespace_packages = true

plugins = ["pydantic.mypy"]

[[tool.mypy.overrides]]
module = "fsspec.*"
ignore_missing_imports = true

[[tool.mypy.overrides]]
module = "BTrees.*"
ignore_missing_imports = true

[[tool.mypy.overrides]]
module = "m3u_parser.*"
ignore_missing_imports = true

[tool.pydantic-mypy]
init_forbid_extra = true
init_typed = true
warn_required_dynamic_aliases = true
warn_untyped_fields = true

[build-system]
requires = ["setuptools","poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"
