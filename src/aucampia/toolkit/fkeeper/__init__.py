#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import base64
import logging
import os
import shutil
import socket
import subprocess
import sys
import uuid
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Any, Optional, TypeVar

import click
import typer
import yaml
from git import Repo
from pydantic import Field
from pydantic.env_settings import SettingsSourceCallable
from pydantic_settings import BaseSettings
from xdg import xdg_config_home, xdg_data_home

logger = logging.getLogger(__name__)

GenericT = TypeVar("GenericT")


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""

apptag = "fkeeper-v0"

cli = typer.Typer()


# class Remote(BaseModel):
#     url: str
#     branch: str

fkeeper_user: Optional[str] = None
fkeeper_group: Optional[str] = None


def is_root() -> bool:
    return os.geteuid() == 0


def default_repo_path() -> Path:
    if is_root():
        return Path("/") / "usr" / "share" / apptag / "repo"
    else:
        return xdg_data_home() / apptag / "repo"


def default_settings_file() -> Path:
    if is_root():
        return Path("/") / "etc" / f"{apptag}.yaml"
    else:
        return xdg_config_home() / f"{apptag}.yaml"


class Settings(BaseSettings):
    auto_push: bool = True
    repo_path: Path = Field(default_factory=default_repo_path)
    # remotes: List[Remote]

    # @classmethod
    # def load(cls) -> "Settings":
    #     settings_file = xdg_config_home() / f"{apptag}.yaml"
    #     logger.debug("settings_file = %s", settings_file)
    #     with settings_file.open() as fileh:
    #         settings_data = yaml.safe_load(fileh)
    #     return cls.parse_obj(settings_data)

    class Config:
        env_prefix = "fkeeper_"

        @classmethod
        def yaml_config_settings_source(cls, settings: BaseSettings) -> dict[str, Any]:
            if "FKEEPER_CONFIG" in os.environ:
                settings_file = Path(os.environ["FKEEPER_CONFIG"])
            else:
                settings_file = xdg_config_home() / f"{apptag}.yaml"
            logger.debug("settings_file = %s", settings_file)
            if not settings_file.exists():
                return {}
            with settings_file.open() as fileh:
                settings_data = yaml.safe_load(fileh)
            assert isinstance(settings_data, dict)
            return settings_data

        @classmethod
        def customise_sources(
            cls,
            init_settings: SettingsSourceCallable,
            env_settings: SettingsSourceCallable,
            file_secret_settings: SettingsSourceCallable,
        ) -> tuple[SettingsSourceCallable, ...]:
            return (
                init_settings,
                env_settings,
                cls.yaml_config_settings_source,
                file_secret_settings,
            )


@dataclass
class FKeeper:
    config: Settings
    repo: Repo

    def add(self, paths: list[Path]) -> None:
        if not self.repo.working_tree_dir:
            raise ValueError("repo has no working tree dir")
        repo_path = Path(self.repo.working_tree_dir)
        repo_node_path = repo_path / get_node_str()
        repo_index = self.repo.index
        remote = self.repo.remote()
        if self.config.auto_push:
            logger.info("pulling changes to %s", remote.name)
            remote.pull()

        for path in paths:
            path_abs = path.absolute()
            path_relative = path_abs.relative_to(path_abs.anchor)
            path_repo = repo_node_path / path_relative
            logger.info("copying %s to %s", path_abs, path_repo)
            path_repo.parent.mkdir(parents=True, exist_ok=True)
            if not path_abs.is_dir():
                result = shutil.copy2(path_abs, path_repo)
            else:
                result = shutil.copytree(path_abs, path_repo, dirs_exist_ok=True)
            logger.info("copy result %s", result)
            repo_index.add([f"{path_repo}"])

        if fkeeper_user or fkeeper_group:
            cuser = fkeeper_user or ""
            cgroup = fkeeper_group or ""
            subprocess.run(f"chown -v -R {cuser}:{cgroup} {repo_node_path}")

        now = datetime.now()
        now_str = now.strftime("%Y-%m-%dT%H:%M:%S")

        if self.repo.is_dirty():
            logger.info("committing changes")
            repo_index.commit(f"added new files {now_str}")
        else:
            logger.info("repo clean, not committing")

        if self.config.auto_push:
            logger.info("pushin changes to %s", remote.name)
            remote.push()
        else:
            logger.info("not pushing changes")

    @classmethod
    def for_settings(cls, settings: Settings) -> "FKeeper":
        repo_path = settings.repo_path
        if repo_path.exists():
            repo = Repo(repo_path)
        else:
            repo = Repo.init(repo_path, mkdir=True)
        return cls(settings, repo)


def machine_id() -> str:
    return Path("/etc/machine-id").read_text()


def get_node_id() -> str:
    return base64.b64encode(uuid.getnode().to_bytes(6, "big")).decode()


def get_node_str() -> str:
    fqdn = socket.getfqdn()
    node_id = get_node_id()
    return f"{fqdn}-{node_id}"


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        logger.getEffectiveLevel(),
    )


@cli.command("add")
def cli_add(
    ctx: typer.Context,
    remove: bool = typer.Option(
        False, "--rm", "-n", help="Remove files after archiving them."
    ),
    path_strings: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    settings = Settings()

    logger.info("settings = %s", settings)

    if len(path_strings) < 1:
        raise click.UsageError("must provide at least one path")

    frepo = FKeeper.for_settings(settings)
    paths = [Path(path_string) for path_string in path_strings]
    # for path in path_strings:
    frepo.add(paths)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
