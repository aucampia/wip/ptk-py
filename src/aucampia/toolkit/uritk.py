import logging
import os
import sys
from collections.abc import Iterable
from pathlib import Path
from urllib.parse import urlsplit
from urllib.request import url2pathname

import typer

logger = logging.getLogger(__name__)


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""


cli = typer.Typer(pretty_exceptions_enable=False)
cli_fileuri = typer.Typer()
cli.add_typer(cli_fileuri, name="fileuri")


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)
    logger.debug(
        "entry ctx_parent_params=%s ctx_params=%s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "log info logging_effective_level=%s",
        logging.getLogger("").getEffectiveLevel(),
    )


@cli_fileuri.command("from-path")
def cli_fileuri_from_path(
    ctx: typer.Context,
    path_strings: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry ctx_parent_params=%s ctx_params=%s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    inputs: Iterable[str] = path_strings
    if not inputs:
        inputs = (line[:-1] for line in sys.stdin)

    for path_string in inputs:
        path = Path(path_string).absolute()
        sys.stdout.write(f"{path.as_uri()}\n")


@cli_fileuri.command("to-path")
def cli_fileuri_to_path(
    ctx: typer.Context,
    fileuris: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry ctx_parent_params=%s ctx_params=%s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    inputs: Iterable[str] = fileuris
    if not inputs:
        inputs = (line[:-1] for line in sys.stdin)

    for fileuri in inputs:
        fileuri_parts = urlsplit(fileuri)
        url2pathname(fileuri_parts.path)
        sys.stdout.write(f"{url2pathname(fileuri_parts.path)}\n")
    # else:
    #     for fileuri in sys.stdin.readlines():
    #         fileuri_parts = urlsplit(fileuri)
    #         url2pathname(fileuri_parts.path)
    #         sys.stdout.write(f"{url2pathname(fileuri_parts.path)}\n")


def main() -> None:
    setup_logging()
    cli()


def setup_logging() -> None:
    logging.basicConfig(
        level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s.%(msecs)03d %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )


if __name__ == "__main__":
    main()
