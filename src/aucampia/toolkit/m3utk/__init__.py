#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

from email.mime import base
import logging
import os
import pathlib
import shutil
import sys
import urllib.parse
from dataclasses import dataclass
from pathlib import Path, PurePath
from typing import Self, TypeVar

import typer

logger = logging.getLogger(__name__)

cli = typer.Typer()


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        logger.getEffectiveLevel(),
    )


@dataclass
class M3UItem:
    base_uri: str
    uri: str

    @property
    def base_path(self) -> PurePath:
        return file_uri_to_path(self.base_uri)

    @property
    def path(self) -> PurePath:
        return PurePath(self.uri)

    @property
    def absolute_path(self) -> Path:
        path = Path(self.uri)
        if path.is_absolute():
            return path
        return Path(self.base_path.parent / path)

    def exists(self) -> bool:
        if self.absolute_path.exists():
            return True
        return False

    @classmethod
    def from_line(cls, base_uri: str, line: str) -> Self:
        return cls(base_uri=base_uri, uri=line.strip())


@dataclass
class M3UList:
    items: list[M3UItem]
    base_uri: str

    @property
    def base_path(self) -> PurePath:
        return file_uri_to_path(self.base_uri)

    @classmethod
    def from_file(cls, path: str | Path) -> Self:
        if isinstance(path, str):
            path = Path(path)
        items = []
        with path.open("r") as f:
            for line in f:
                items.append(M3UItem.from_line(path.absolute().as_uri(), line))
        return cls(base_uri=path.absolute().as_uri(), items=items)


PurePathT = TypeVar("PurePathT", bound=PurePath)


def file_uri_to_path(
    file_uri: str, path_class: type[PurePathT] = pathlib.PurePath  # type: ignore[assignment]
) -> PurePathT:
    """
    This function returns a pathlib.PurePath object for the supplied file URI.

    :param str file_uri: The file URI ...
    :param class path_class: The type of path in the file_uri. By default it uses
        the system specific path pathlib.PurePath, to force a specific type of path
        pass pathlib.PureWindowsPath or pathlib.PurePosixPath
    :returns: the pathlib.PurePath object
    :rtype: pathlib.PurePath
    """
    windows_path = isinstance(path_class(), pathlib.PureWindowsPath)
    file_uri_parsed = urllib.parse.urlparse(file_uri)
    file_uri_path_unquoted = urllib.parse.unquote(file_uri_parsed.path)
    if windows_path and file_uri_path_unquoted.startswith("/"):
        result = path_class(file_uri_path_unquoted[1:])
    else:
        result = path_class(file_uri_path_unquoted)
    if not result.is_absolute():
        raise ValueError(
            f"Invalid file uri {file_uri} : resulting path {result} not absolute"
        )
    return result


@cli.command("check")
def cli_check(
    ctx: typer.Context,
    path_strings: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    fail = False

    for path in path_strings:
        m3u = M3UList.from_file(path)
        for item in m3u.items:
            if not item.exists():
                logging.error(f"MISSING: {item.uri}")
                fail = True
            else:
                logging.info(f"OK: {item.uri}")
        logging.info(f"{path = }: OK")

    if fail:
        sys.exit(1)


@cli.command("copy")
def cli_copy(
    ctx: typer.Context,
    copy_dirs: bool = typer.Option(False, "--dirs", "-d", help="copy directories"),
    source: str = typer.Argument(..., help="source m3u file"),
    destination: str = typer.Argument(..., help="destination path"),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    source_path = Path(source)
    source_m3u = M3UList.from_file(source_path)
    destination_path = Path(destination)
    destination_path.mkdir(parents=True, exist_ok=True)

    def copy_item(item: M3UItem) -> None:
        target_path = destination_path / item.path
        target_path.parent.mkdir(parents=True, exist_ok=True)
        logging.info(f"copying {item.absolute_path!r} to {target_path!r}")
        shutil.copy(item.absolute_path, target_path)

    if not copy_dirs:
        for item in source_m3u.items:
            copy_item(item)
    else:
        dirs: set[tuple[Path, Path]] = set()
        for item in source_m3u.items:
            if len(item.path.parts) > 1:
                dirs.add((item.absolute_path.parent, destination_path / item.path.parent))
            else:
                copy_item(item)
        for source_dir, destination_dir in dirs:
            logging.info(f"copying dir {source_dir!r} to {destination_dir!r}")
            shutil.copytree(source_dir, destination_dir, dirs_exist_ok=True)

    logging.info(f"copying {source_path} to {destination_path / source_path.name}")
    shutil.copy(source_path, destination_path / source_path.name)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
