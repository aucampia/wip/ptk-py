#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import csv
import logging
import os
import re
import sys
from collections import defaultdict
from datetime import datetime
from pathlib import Path
from typing import Optional

import click
import typer

logger = logging.getLogger(__name__)

cli = typer.Typer()


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        logger.getEffectiveLevel(),
    )


fmeta_pattern = re.compile(
    r"^(?:(?P<filename>.*)[@:])?(?P<predicate>tag|score)[@:](?P<value>[^@:]+)$"
)


@cli.command("ls")
def cli_ls(
    ctx: typer.Context,
    zero_termed: bool = typer.Option(False, "-z"),
    tag_filter_str: Optional[str] = typer.Option(None, "--tag-filter", "-t"),
    fields_str: Optional[str] = typer.Option(
        None, "--field", "-f", help="fields to display"
    ),
    path_strings: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    tag_filter = re.compile(tag_filter_str) if tag_filter_str is not None else None

    if len(path_strings) < 1:
        raise click.UsageError("must provide at least one path")

    if not fields_str:
        fields = ["score", "mtime", "path", "tags"]
    else:
        fields = fields_str.split(",")

    writer = csv.DictWriter(
        sys.stdout,
        fieldnames=fields,
        delimiter="\t",
        lineterminator=chr(0) if zero_termed else "\n",
        extrasaction="ignore",
    )

    for root, dirs, files in os.walk(path_strings[0]):
        root_path = Path(root)
        logging.debug("root = %s, dirs = %s, files = %s", root, dirs, files)
        entities: dict[str | None, dict[str, list[str]]] = defaultdict(
            lambda: defaultdict(list)
        )
        for file in files:
            match = fmeta_pattern.match(file)
            if match:
                logging.debug("match = %s", match.groupdict())
                entities[match.group("filename")][match.group("predicate")].append(
                    match.group("value")
                )
        logging.debug("entities = %s", entities)
        for entity in entities:
            if entity is None:
                entity_path = root_path
            else:
                entity_path = root_path / entity
            tags = entities[entity].get("tag", [])
            scores = sorted(entities[entity].get("score", []))
            if scores:
                score = scores[-1]
            else:
                score = " "
            entity_stat = None
            entity_mtime = None
            if entity_path.exists():
                entity_stat = entity_path.stat()
                entity_mtime = datetime.fromtimestamp(entity_stat.st_mtime_ns / 1e9)

            record = {
                "score": str(score),
                "mtime": entity_mtime.strftime("%Y-%m-%dT%H:%M:%S.%f")
                if entity_mtime
                else " ",
                "path": str(entity_path),
                "tags": ",".join(tags),
            }

            # parts = [
            #     f"{score}",
            #     entity_mtime.strftime("%Y-%m-%dT%H:%M:%S.%f"),
            #     f"{entity_path}",
            #     f"tags={",".join(tags)}",
            # ]
            if tag_filter is not None:
                matched = False
                for tag in tags:
                    if tag_filter.match(tag):
                        matched = True
                        break
                if not matched:
                    continue

            writer.writerow(record)

            # sys.stdout.write(f"{'\t'.join(parts)}{chr(0) if zero_termed else '\n'}")


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
