#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=88 cc=+1:
# vim: set filetype=python tw=88 cc=+1:

import logging
import os
import re
import sys
import typing as t
from collections.abc import MutableSet
from dataclasses import dataclass, field
from enum import Enum, auto
from pathlib import Path
from re import Pattern
from typing import Optional

import click
import typer
import yaml
from BTrees.OOBTree import TreeSet
from dacite import from_dict as dc_from_dict
from xdg import xdg_config_home

from .utils import as_paths

LOGGER = logging.getLogger(__name__)

GenericT = t.TypeVar("GenericT")


"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""

apptag = "ftf-v0"

cli = typer.Typer()


@dataclass
class Exclude:
    id: str
    default: bool = False
    names: t.MutableSequence[str] = field(default_factory=lambda: [])


class ItemType(Enum):
    FILE = auto()
    DIR = auto()


@dataclass
class ExcludeSet:
    excludes: t.MutableSequence[Exclude] = field(default_factory=lambda: [])


@dataclass
class FileType:
    id: str
    aliases: t.MutableSet[str] = field(default_factory=lambda: set())
    # excludes: t.MutableSequence[Exclude] = field(default_factory=lambda: [])
    exclude_set: ExcludeSet = field(default_factory=lambda: ExcludeSet())
    extensions: Optional[t.MutableSet[str]] = None
    categories: t.MutableSet[str] = field(default_factory=lambda: set())
    patterns: Optional[MutableSet[Pattern[str]]] = None

    def matches(self, dirname: str, basename: str) -> t.Optional[str]:
        def rv() -> str:
            return os.path.join(dirname, basename)

        if self.extensions is not None:
            okay = False
            for extension in self.extensions:
                if basename.endswith(extension):
                    okay = True
            if not okay:
                return None

        if self.patterns is not None:
            okay = False
            for pattern in self.patterns:
                if pattern.match(basename):
                    okay = True
            if not okay:
                return None

        return rv()


@dataclass
class Config:
    excludes: t.MutableMapping[str, Exclude] = field(default_factory=lambda: {})
    default_excludes: t.MutableSequence[Exclude] = field(default_factory=lambda: [])
    file_types: t.MutableMapping[str, FileType] = field(default_factory=lambda: {})

    @classmethod
    def _parse_excludes(
        cls,
        values: t.Sequence[dict[str, t.Any]],
        lookups: t.Sequence[t.Mapping[str, Exclude]],
    ) -> t.MutableSequence[Exclude]:
        result = []

        def resolve_ref(ref: str) -> t.Optional[Exclude]:
            for lookup in lookups:
                if ref in lookup:
                    return lookup[ref]
            return None

        for data in values:
            exclude: Exclude
            if "ref" in data:
                if len(data.keys()) > 1:
                    raise ValueError("exclude with ref cannot have other keys")
                ref = data["ref"]
                tmp = resolve_ref(ref)
                if tmp is None:
                    raise ValueError(f"could not resolve ref {ref}")
                exclude = tmp
            else:
                exclude = dc_from_dict(data_class=Exclude, data=data)
            result.append(exclude)
        return result

    @classmethod
    def from_mapping(cls, data: t.Mapping[str, t.Any]) -> "Config":
        result = cls()
        for key, value_dict in data["excludes"].items():
            exclude = dc_from_dict(data_class=Exclude, data={"id": key, **value_dict})
            result.excludes[key] = exclude
            if exclude.default:
                result.default_excludes.append(exclude)
        for key, value_dict in data["file_types"].items():
            file_type = FileType(key)
            file_type.aliases = set(value_dict["aliases"])
            file_type.extensions = (
                None
                if "extensions" not in value_dict
                else set(value_dict["extensions"])
            )
            file_type.patterns = (
                None
                if "patterns" not in value_dict
                else {re.compile(pattern) for pattern in value_dict["patterns"]}
            )
            file_type.categories = set(value_dict["categories"])
            file_type.exclude_set = ExcludeSet(
                cls._parse_excludes(value_dict["excludes"], [result.excludes])
            )
            result.file_types[key] = file_type
        return result

    def select_filetypes(
        self,
        selected_file_types: set[str],
        selected_categories: set[str],
    ) -> t.Sequence[FileType]:
        result = []
        for _, file_type in self.file_types.items():
            if selected_file_types:
                file_type_okay = (file_type.id in selected_file_types) or (
                    not file_type.aliases.isdisjoint(selected_file_types)
                )
            else:
                file_type_okay = True

            if selected_categories:
                category_okay = not file_type.categories.isdisjoint(selected_categories)
            else:
                category_okay = True
            logging.debug(
                "file_type_okay = %s, category_okay = %s", file_type_okay, category_okay
            )
            if file_type_okay and category_okay:
                result.append(file_type)
                continue

        return result

    @classmethod
    def _filter_items(
        cls,
        excludes: t.Sequence[Exclude],
        visited: set[tuple[int, int]],
        root: str,
        items: t.MutableSequence[str],
        item_type: ItemType,
    ) -> None:
        for index in reversed(range(len(items))):
            item = items[index]
            # deleted = False
            for exclude in excludes:
                if item in exclude.names:
                    if index > (len(items) - 1):
                        raise RuntimeError(f"{index} > len({items})")
                    del items[index]
                    # deleted = True
                    break

    @classmethod
    def _filter_children(
        cls,
        excludes: t.Sequence[Exclude],
        visited: set[tuple[int, int]],
        root: str,
        dirs: t.MutableSequence[str],
        files: t.MutableSequence[str],
    ) -> None:
        cls._filter_items(excludes, visited, root, dirs, ItemType.DIR)
        cls._filter_items(excludes, visited, root, files, ItemType.FILE)

    @classmethod
    def _match_file_types(
        cls, file_types: t.Sequence[FileType], dirname: str, basename: str
    ) -> t.Optional[str]:
        for file_type in file_types:
            result = file_type.matches(dirname, basename)
            if result is not None:
                return result

        return None

    def find_files(
        self,
        paths: t.Sequence[Path],
        selected_file_types: set[str],
        selected_categories: set[str],
    ) -> t.Generator[str, None, None]:
        file_types = self.select_filetypes(selected_file_types, selected_categories)
        if not file_types:
            raise click.UsageError(
                f"no file_types matched {selected_file_types} and {selected_categories}"
            )
        logging.debug("file_types = %s", file_types)
        excludes = [exclude for ft in file_types for exclude in ft.exclude_set.excludes]
        excludes.extend(self.default_excludes)
        logging.debug("excludes = %s", excludes)
        visited: set[tuple[int, int]] = set()
        for path in paths:
            for root, dirs, files in os.walk(path, followlinks=True):
                root_stat = os.stat(root)
                root_key = (root_stat.st_dev, root_stat.st_ino)
                if root_key in visited:
                    logging.debug("already visited %s", root)
                    dirs.clear()
                    files.clear()
                    continue
                visited.add(root_key)
                self._filter_children(excludes, visited, root, dirs, files)
                logging.debug("root = %s, dirs = %s", root, dirs)
                logging.debug("root = %s, files = %s", root, files)
                for filei in files:
                    matched = self._match_file_types(file_types, root, filei)
                    if matched is not None:
                        yield matched


@cli.command()
def cli_command(
    ctx: typer.Context,
    verbosity: int = typer.Option(0, "--verbose", "-v", count=True),
    file_types: list[str] = typer.Option([], "--type", "-t"),
    categories: list[str] = typer.Option([], "--category", "-c"),
    null_delimited: bool = typer.Option(False, "--null-data", "-z", "-0", is_flag=True),
    sort: bool = typer.Option(False, "--sort", "-s", is_flag=True),
    path_strings: list[str] = typer.Argument(None),
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    LOGGER.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    LOGGER.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        LOGGER.getEffectiveLevel(),
    )

    config_file = xdg_config_home() / f"{apptag}.yaml"
    LOGGER.debug("%s", {"config_file": config_file})
    with config_file.open() as fileh:
        config_data = yaml.safe_load(fileh)
    config = Config.from_mapping(config_data)
    LOGGER.debug("%s", {"config": config})

    if path_strings is None or not path_strings:
        path_strings = ["."]
    paths = as_paths(path_strings)
    delim = "\000" if null_delimited else "\n"
    outfiles = config.find_files(
        paths=paths,
        selected_file_types=None if file_types is None else set(file_types),
        selected_categories=None if categories is None else set(categories),
    )

    if sort:
        outfiles_sorted = TreeSet()
        for file in outfiles:
            outfiles_sorted.add(file)
        outfiles = outfiles_sorted

    for file in outfiles:
        # if not first:
        #     sys.stdout.write(delim)
        # else:
        #     first = False
        sys.stdout.write(file)
        sys.stdout.write(delim)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
