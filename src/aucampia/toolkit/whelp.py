# https://click.palletsprojects.com/en/7.x/api/#parameters
# https://click.palletsprojects.com/en/7.x/options/
# https://click.palletsprojects.com/en/7.x/arguments/
# mypy: allow-untyped-decorators
import abc
import collections
import logging
import os
import pathlib
import shlex
import subprocess
import sys
import typing
from dataclasses import dataclass

import click
import yaml
from dacite import from_dict as dc_from_dict
from xdg import xdg_config_home, xdg_data_home

from .utils import as_path, coalescex, vdict

LOGGER = logging.getLogger(__name__)

apptag = "whelp-v0"


class WhelpConfigABC(abc.ABC):
    @property
    @abc.abstractproperty
    def data_path(self) -> pathlib.Path:
        ...


@dataclass
class WhelpConfigFile:
    data_path: typing.Optional[pathlib.Path]
    source: typing.Optional[pathlib.Path]

    @classmethod
    def from_dict(cls, data: dict[str, typing.Any]) -> typing.Self:
        return dc_from_dict(data_class=cls, data=data)

    @classmethod
    def from_yaml_file(cls, file_path: pathlib.Path) -> typing.Self:
        with file_path.open() as fileh:
            data = yaml.safe_load(fileh)
        result = cls.from_dict(data)
        result.source = file_path
        return result

    @classmethod
    def determine_path(cls, options: "WhelpOptions") -> pathlib.Path:
        if options.config_file is not None:
            return options.config_file
        if tmp := as_path(os.environ.get("WHELP_CONFIG_PATH", None)):
            return tmp
        return xdg_config_home() / f"{apptag}.yaml"

    @classmethod
    def create(cls, options: "WhelpOptions") -> "WhelpConfigFile":
        path = cls.determine_path(options)
        if path.exists():
            return cls.from_yaml_file(path)
        else:
            return cls(None, None)


class WhelpConfig(WhelpConfigABC):
    config_file: WhelpConfigFile

    def __init__(self, config_file: WhelpConfigFile) -> None:
        self.config_file = config_file

    @property
    def data_path(self) -> pathlib.Path:
        return coalescex(
            [self.config_file.data_path, as_path(os.environ.get("WHELP_DATA_PATH"))],
            xdg_data_home() / apptag,
        )

    @property
    def prefix_path(self) -> pathlib.Path:
        return self.data_path / "prefix"


@dataclass
class WhelpOptions:
    config_file: typing.Optional[pathlib.Path]


@dataclass
class WhelpContext:
    options: WhelpOptions
    config_file: WhelpConfigFile
    config: WhelpConfig

    def path_for_prefix(self, prefix_name: str) -> pathlib.Path:
        return self.config.prefix_path / prefix_name


def parse_env(spec: str) -> tuple[str, str]:
    parts = spec.split("=", 1)
    if len(parts) != 2:
        raise ValueError(f"Could not split {parts} on '='")
    return (parts[0], parts[1])


def parse_env_set(specs: list[str]) -> dict[str, str]:
    result: dict[str, str] = collections.OrderedDict()
    for spec in specs:
        (key, value) = parse_env(spec)
        result[key] = value
    return result


def parse_env_add(specs: list[str]) -> dict[str, list[str]]:
    result: dict[str, list[str]] = collections.OrderedDict()
    for spec in specs:
        (key, value) = parse_env(spec)
        result.setdefault(key, [])
        result[key].append(value)
    return result


def env_extend(
    env: typing.MutableMapping[str, str], key: str, values: list[str]
) -> typing.MutableMapping[str, str]:
    if len(values) < 0:
        return env
    prefix = os.pathsep.join(values)
    suffix = ""
    if key in env:
        suffix = f"{os.pathsep}{env[key]}"
    env[key] = f"{prefix}{suffix}"
    return env


config_file_name = "config.yaml"

# config.data_path / "prefix" / name / wine
EnvValueT = typing.Union[str, list[str]]


@dataclass(eq=False)
class WhelpPrefix:
    context: WhelpContext
    name: str
    # env: typing.Dict[str, str]
    env_set: dict[str, str]
    env_add: dict[str, list[str]]
    _wineprefix: typing.Optional[pathlib.Path] = None

    @property
    def base_path(self) -> pathlib.Path:
        return self.context.path_for_prefix(self.name)

    @classmethod
    def from_name(cls, context: WhelpContext, name: str) -> "WhelpPrefix":
        base_path = context.path_for_prefix(name)
        config_file = base_path / config_file_name
        return cls.from_config_file(context, name, config_file)

    @classmethod
    def exists(cls, context: WhelpContext, name: str) -> bool:
        base_path = context.path_for_prefix(name)
        config_file = base_path / config_file_name
        return config_file.exists()

    @classmethod
    def from_config_file(
        cls, context: WhelpContext, name: str, config_file: pathlib.Path
    ) -> "WhelpPrefix":
        with config_file.open() as fileh:
            config_data = yaml.safe_load(fileh)
            return cls.from_config_data(context, name, config_data)

    def save(self) -> None:
        self.config_file.parent.mkdir(parents=True, exist_ok=True)
        with self.config_file.open("w+") as fileh:
            config_data = {
                "name": self.name,
                "env_set": dict(self.env_set),
                "env_add": dict(self.env_add),
                "wineprefix": self._wineprefix,
            }
            yaml.safe_dump(config_data, fileh)

    @classmethod
    def from_config_data(
        cls, context: WhelpContext, name: str, config_data: typing.Any
    ) -> "WhelpPrefix":
        return WhelpPrefix(
            context=context,
            name=name,
            env_set=config_data["env_set"],
            env_add=config_data["env_add"],
            _wineprefix=config_data["wineprefix"],
        )

    @property
    def config_file(self) -> pathlib.Path:
        return self.base_path / "config.yaml"

    @property
    def initialized(self) -> bool:
        return self.config_file.exists()

    def init(self, force: bool = False) -> None:
        logging.info("base_path = %s", self.base_path)
        logging.info("env_set = %s", self.env_set)
        logging.info("env_add = %s", self.env_add)
        logging.info("wineprefix = %s", self.wineprefix)
        if self.initialized:
            msg = f"{self.name} already initialized at {self.base_path} ..."
            if force:
                logging.warn(msg)
            else:
                raise RuntimeError(msg)
        self.base_path.mkdir(parents=True, exist_ok=True)
        self.save()

        self.run(["wine", "whoami", "-n", "8.8.8.8"])
        """
        osenv = self.osenv()
        log_call_args(
            logging.info,
            subprocess.run,
            ["wine", "ping", "-n", "8.8.8.8"],
            env=osenv,
        )
        """

    @property
    def wineprefix(self) -> pathlib.Path:
        return coalescex([self._wineprefix], self.base_path / "wine")

    def osenv(
        self,
        *,
        extra_set: typing.Optional[typing.Mapping[str, str]] = None,
        extra_add: typing.Optional[typing.Mapping[str, list[str]]] = None,
        with_os: bool = True,
    ) -> dict[str, str]:
        new_env: dict[str, str]
        if with_os:
            new_env = dict(os.environ)
        else:
            new_env = {}

        for env_key, env_value in self.env_set.items():
            new_env[env_key] = str(env_value)
        for env_key, env_add in self.env_add.items():
            env_extend(new_env, env_key, env_add)

        if extra_add is None:
            extra_add = {}
        if extra_set is None:
            extra_set = {}

        for env_key, env_value in extra_set.items():
            new_env[env_key] = str(env_value)
        for env_key, env_add in extra_add.items():
            env_extend(new_env, env_key, env_add)

        new_env["WINEPREFIX"] = str(self.wineprefix)
        return new_env

    def exec(
        self,
        args: list[str],
        *,
        extra_set: typing.Optional[typing.Mapping[str, str]] = None,
        extra_add: typing.Optional[typing.Mapping[str, list[str]]] = None,
    ) -> None:
        osenv = self.osenv(extra_set=extra_set, extra_add=extra_add)
        logging.info("will exec: %s", shlex.join(args))
        os.execvpe(args[0], args, osenv)

    def run(
        self,
        args: list[str],
        *,
        extra_set: typing.Optional[typing.Mapping[str, str]] = None,
        extra_add: typing.Optional[typing.Mapping[str, list[str]]] = None,
    ) -> None:
        osenv = self.osenv(extra_set=extra_set, extra_add=extra_add)
        logging.info("will run: %s", shlex.join(args))
        subprocess.run(args, env=osenv)


@click.group(name="whelp")
@click.option("--config-file", default=None, required=False, type=str)
@click.pass_context
def whelp(ctx: click.Context, config_file: typing.Optional[str]) -> None:
    LOGGER.debug("entry: %s", vdict("config_file"))
    options = WhelpOptions(as_path(config_file))
    config_file_obj = WhelpConfigFile.create(options)
    config = WhelpConfig(config_file_obj)
    context = WhelpContext(options, config_file_obj, config)
    ctx.obj = context
    LOGGER.debug("ctx = %s, ctx.obj = %s", ctx, ctx.obj)


@whelp.command(name="init")
@click.option("--name", "-n", required=True, type=str)
@click.option(
    "--env-set", "--es", multiple=True, default=[], type=str, metavar="KEY=VALUE"
)
@click.option(
    "--env-add", "--ea", multiple=True, default=[], type=str, metavar="KEY=VALUE"
)
@click.option("--wine-prefix", "--wp", default=None, type=str)
@click.pass_context
def init(
    ctx: click.Context,
    name: str,
    env_set: list[str],
    env_add: list[str],
    wine_prefix: typing.Optional[str],
) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)

    env_set_dict = parse_env_set(env_set)
    env_add_dict = parse_env_add(env_add)
    context = ctx.obj
    if WhelpPrefix.exists(context, name):
        logging.info("Already initialized ...")
        return
    prefix = WhelpPrefix(
        context,
        name=name,
        env_set=env_set_dict,
        env_add=env_add_dict,
        _wineprefix=as_path(wine_prefix),
    )
    prefix.init()


@whelp.command(name="info")
@click.option("--name", "-n", required=False, type=str)
@click.pass_context
def info(ctx: click.Context, name: typing.Optional[str]) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)

    sys.stderr.write(f"prefix_path = {ctx.obj.config.prefix_path}\n")

    if name is not None:
        prefix = WhelpPrefix.from_name(ctx.obj, name)
        sys.stderr.write(f"name = {prefix.name}\n")
        sys.stderr.write(f"base_dir = {prefix.base_path}\n")
        sys.stderr.write(f"config_file = {prefix.config_file}\n")
        sys.stderr.write(f"wineprefix = {prefix.wineprefix}\n")


@whelp.command(name="exec")
@click.option("--name", "-n", required=True, type=str)
@click.option(
    "--env-set", "--es", multiple=True, default=[], type=str, metavar="KEY=VALUE"
)
@click.option(
    "--env-add", "--ea", multiple=True, default=[], type=str, metavar="KEY=VALUE"
)
@click.argument("args", required=True, nargs=-1)
@click.pass_context
def exec(
    ctx: click.Context,
    name: str,
    args: list[str],
    env_set: list[str],
    env_add: list[str],
) -> None:
    LOGGER.debug("entry ...")
    LOGGER.debug("ctx = %s", ctx)

    prefix = WhelpPrefix.from_name(ctx.obj, name)
    prefix.exec(
        args, extra_set=parse_env_set(env_set), extra_add=parse_env_add(env_add)
    )
