#!/usr/bin/env python3
from __future__ import annotations

import logging
import os
import shlex
import subprocess
import sys
from collections.abc import Iterable, Iterator
from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path, PurePath
from typing import TypeVar, Union

import typer
import yaml
from pydantic import BaseModel, validator
from pydantic.fields import Field
from xdg import xdg_config_home

logger = logging.getLogger(__name__)

GenericT = TypeVar("GenericT")

__all__: list[str] = []

"""
https://click.palletsprojects.com/en/7.x/api/#parameters
https://click.palletsprojects.com/en/7.x/options/
https://click.palletsprojects.com/en/7.x/arguments/
https://typer.tiangolo.com/
https://typer.tiangolo.com/tutorial/options/
"""

apptag = "vscnotes-v0"

PathLike = Union[PurePath, str]
PathLikeT = TypeVar("PathLikeT", bound=PathLike)


@contextmanager
def ctx_chdir(newdir: PathLikeT) -> Iterator[PathLikeT]:
    cwd = os.getcwd()
    try:
        os.chdir(f"{newdir}")
        yield newdir
    finally:
        os.chdir(cwd)


class Config(BaseModel):
    notes_path: Path
    exclude_patterns: list[str] = Field(default_factory=lambda: [])
    user_data_dir: Path = Field(
        default_factory=lambda: xdg_config_home() / "Code-notes"
    )
    extensions_dir: Path = Field(
        default_factory=lambda: Path.home() / ".vscode/extensions-notes"
    )

    @validator("notes_path")
    @classmethod
    def expand_notes_path(cls, v: Path) -> Path:
        return v.expanduser().resolve()

    @classmethod
    def load(cls) -> Config:
        config_file = xdg_config_home() / f"{apptag}.yaml"
        logger.debug("config_file = %s", config_file)
        with config_file.open() as fileh:
            config_data = yaml.safe_load(fileh)
        return cls.parse_obj(config_data)

    def rebase_path_args(self, path_args: list[str]) -> list[str]:
        result = []
        for path_arg in path_args:
            path = os.path.abspath(path_arg)
            logger.debug("path = %s", path)
            result.append(os.path.relpath(path, self.notes_path))
        return result

    def run_code(self, args: list[str]) -> None:
        logger.debug("args = %s", args)
        args = [
            "--reuse-window",
            # "--user-data-dir",
            # f"{self.user_data_dir}",
            # "--extensions-dir",
            # f"{self.extensions_dir}",
            f"{self.notes_path}",
            *args,
        ]
        logger.debug("args = %s", shlex.join(["code", *args]))
        os.execvp("code", args)

    def iter_files(self) -> Iterable[str]:
        def _check_name(root: str, item: str) -> bool:
            if (
                item.startswith(".git")
                or item.startswith(".obsidian")
                or item.startswith(".vscode")
                or item == "trash"
            ):
                return False
            return True

        visited: set[tuple[int, int]] = set()
        for root, dirs, files in os.walk(self.notes_path, followlinks=True):
            root_stat = os.stat(root)
            root_key = (root_stat.st_dev, root_stat.st_ino)
            if root_key in visited:
                logging.debug("already visited %s", root)
                dirs.clear()
                files.clear()
                continue
            dirs[:] = [diri for diri in dirs if _check_name(root, diri)]
            visited.add(root_key)
            yield from (
                os.path.join(root, filei) for filei in files if _check_name(root, filei)
            )

    def get_files(self) -> list[str]:
        return sorted(self.iter_files())


@dataclass
class Context:
    config: Config


cli = typer.Typer()
cli_sub = typer.Typer()
cli.add_typer(cli_sub, name="sub")


@cli.callback()
def cli_callback(
    ctx: typer.Context, verbosity: int = typer.Option(0, "--verbose", "-v", count=True)
) -> None:
    if verbosity is not None:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        new_level = (
            root_logger.getEffectiveLevel()
            - (min(1, verbosity)) * 10
            - min(max(0, verbosity - 1), 9) * 1
        )
        root_logger.setLevel(new_level)

    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )
    logger.debug(
        "logging.level = %s, LOGGER.level = %s",
        logging.getLogger("").getEffectiveLevel(),
        logger.getEffectiveLevel(),
    )
    ctx.obj = Context(config=Config.load())


@cli.command("open", help="Runs the vscode command with provided arguments")
def cli_open(
    ctx: typer.Context,
    args: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    context: Context = ctx.find_object(Context)
    logger.debug("context = %s", context)

    context.config.run_code(args)


@cli.command("list")
def cli_list(
    ctx: typer.Context,
    open: bool = typer.Option(False, "--open", "-o"),
    grep_args: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    context: Context = ctx.find_object(Context)
    logger.debug("context = %s", context)

    files = context.config.get_files()
    if grep_args:
        args = ["grep", "-i", *grep_args]
        logger.debug("args = %s", shlex.join(["code", *args]))
        result = subprocess.run(
            args, input="\n".join(files), stdout=subprocess.PIPE, encoding="utf-8"
        )
        files = result.stdout.splitlines()

    if open:
        context.config.run_code(files)
    for file in files:
        print(file)


@cli.command("grep")
def cli_grep(
    ctx: typer.Context,
    grep_args: list[str] = typer.Argument(None),
) -> None:
    logger.debug(
        "entry: ctx.parent.params = %s, ctx.params = %s",
        ({} if ctx.parent is None else ctx.parent.params),
        ctx.params,
    )

    context: Context = ctx.find_object(Context)
    logger.debug("context = %s", context)

    files = context.config.get_files()
    if grep_args:
        args = ["grep", "-i", *grep_args, "--", *files]
        logger.debug("args = %s", shlex.join(["code", *args]))
        subprocess.run(args)


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("PYLOGGING_LEVEL", logging.INFO),
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s.%(msecs)03d %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    cli()


if __name__ == "__main__":
    main()
