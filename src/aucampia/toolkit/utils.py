import inspect
import typing
import typing as t
from pathlib import Path, PurePath

GenericT = t.TypeVar("GenericT")


def vdict(*keys: str, obj: t.Any = None) -> dict[str, t.Any]:
    if obj is None:
        lvars = t.cast(t.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, None) for key in keys}


def coalesce(*args: t.Optional[GenericT]) -> t.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None


def coalescex(
    options: t.Iterable[t.Optional[GenericT]], fallback: GenericT
) -> GenericT:
    for option in options:
        if option is not None:
            return option
    return fallback


PurePathOrStr = t.Union[str, PurePath]
PathOrStr = t.Union[str, Path]

GenericPathT = t.TypeVar("GenericPathT", bound="PurePath")
GenericPathOrStrT = t.Union[str, PurePath]


@typing.overload
def as_path(value: PathOrStr) -> Path:
    ...


@typing.overload
def as_path(value: t.Optional[PathOrStr]) -> t.Optional[Path]:
    ...


def as_path(value: t.Optional[PathOrStr]) -> t.Optional[Path]:
    if value is None:
        return None
    if isinstance(value, str):
        return Path(value)
    else:
        return value


@typing.overload
def as_paths(values: t.Sequence[PathOrStr]) -> t.Sequence[Path]:
    ...


@typing.overload
def as_paths(values: t.Sequence[t.Optional[PathOrStr]]) -> t.Sequence[t.Optional[Path]]:
    ...


def as_paths(values: t.Sequence[t.Optional[PathOrStr]]) -> t.Sequence[t.Optional[Path]]:
    result = []
    for value in values:
        result.append(as_path(value))
    return result


def log_call(
    log_function: t.Callable[..., None],
    function: t.Callable[..., GenericT],
    *args: t.Any,
    **kwargs: t.Any,
) -> GenericT:
    log_function("will call '%s' with %s, %s", function.__name__, args, kwargs)
    return function(*args, **kwargs)


def log_call_args(
    log_function: t.Callable[..., None],
    function: t.Callable[..., GenericT],
    *args: t.Any,
    **kwargs: t.Any,
) -> GenericT:
    log_function("will call '%s' with args=%s", function.__name__, args)
    return function(*args, **kwargs)
