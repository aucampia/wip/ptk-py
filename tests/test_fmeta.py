import logging

import pytest
from aucampia.toolkit.fmeta import fmeta_pattern


@pytest.mark.parametrize(
    "filename",
    [
        "some file @tag@10",
        "some file:tag:10",
        "tag:10",
        "tag@10",
        "score:10",
        "score@10",
        "some file @score@10",
        "some file:score:10",
        "score@10",
        "some file",
    ],
)
def test_pattern(filename: str) -> None:
    match = fmeta_pattern.match(filename)
    logging.debug("match = %s", match)
    if match is not None:
        logging.debug("match.groupdict() = %s", match.groupdict())
